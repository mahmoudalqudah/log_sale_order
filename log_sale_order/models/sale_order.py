from odoo import models , fields , api
from datetime import  datetime


class edit_sale_order(models.Model):
    _inherit = "sale.order"


    @api.model
    def create(self, vals):
        res = super(edit_sale_order, self).create(vals)

        log_data={"user_n":self._uid,
                        "action_date":datetime.today(),
                        "action": "Create"}
        self.env["sale.log"].create(log_data)
        return res

    @api.multi
    def write(self, vals):
        res = super(edit_sale_order, self).write(vals)

        log_data = {"user_n": self._uid,
                    "action_date": datetime.today(),
                    "action": "Update"}
        self.env["sale.log"].create(log_data)
        return res

    @api.multi
    def unlink(self):
        res = super(edit_sale_order, self).unlink()

        log_data = {"user_n": self._uid,
                    "action_date": datetime.today(),
                    "action": "Delete"}
        self.env["sale.log"].create(log_data)
        return res