from odoo import models , fields , api

class saleLog(models.Model):
    _name = "sale.log"

    action = fields.Char("Action")
    action_date = fields.Datetime("Action Date")
    user_n = fields.Many2one('res.users','User')

